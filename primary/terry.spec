Name: terry
Version: {{{ git_dir_version }}}
Release: 1%{?dist}
Summary: Packages I use for my environment.

License: MIT
BuildArch: noarch

VCS: {{{ git_dir_vcs }}}

# This is an abuse of Obsoletes
# But I don't like PackageKit and want to make sure it is removed
Obsoletes: PackageKit-glib

# This package adds repos that are used to provide some of the Packages
Requires: terry-repos

# Core Packages
Requires: zsh
Requires: terry-editor
Requires: kitty-terminfo
Requires: bash
Requires: fira-code-fonts

# Languages
Requires: python3
Requires: python3-devel
Requires: luajit
Requires: zig
Requires: rust
Requires: nodejs
Requires: gcc
Requires: gcc-c++

# Package managers
Requires: cargo
Requires: luarocks
Requires: npm
Requires: pipx
Requires: python3-pip

# Sync
Requires: nautilus-dropbox

# CLIs for daily use
Requires: bat
Requires: entr
Requires: fd-find
Requires: jq
Requires: ripgrep
Requires: thefuck
Requires: tldr
Requires: fzf
Requires: stow
Requires: exa
Requires: python3-build
Requires: python3-bpython
Requires: python3-ptpython
Requires: rpm-build
Requires: rpmlint
Requires: rpmdevtools

Requires: git
Requires: git-delta
Requires: git-extras
Requires: git-subtree

# TUIs for checking various things
Requires: ncdu
Requires: ranger
Requires: htop
Requires: bpytop
Requires: httpie
Requires: neofetch
Requires: prettyping

# Deps for scripts
Requires: python3-GitPython
Requires: python3-more-itertools
Requires: python3-sh
Requires: python3-typer
Requires: python3-box
Requires: python3-wheel
Requires: python3-anyio

# TUI makers
Requires: python3-rich
Recommends: python3-textual >= 0.9

# fuc stuff
Requires: cowsay
Requires: fortune-mod

%files -n terry

%description
Packages I use in my environment.

%package gui
Summary: Packages I use in my GUI environments.


Requires: ImageMagick
Requires: arandr
Requires: copyq
Requires: dunst
Requires: feh
Requires: flameshot
Requires: i3lock
Requires: kitty
Requires: network-manager-applet
Requires: redshift
Requires: rofi
Requires: udiskie
Requires: volumeicon
Requires: xfce-polkit
Requires: xfce4-power-manager
Requires: xfce4-volumed
Requires: qtile

Requires: python3-pyaudio
Requires: python3-gobject
Requires: python3-dbus

%files -n %{name}-gui

%description gui
Packages I use in my GUI environments.

%package home
Summary: Packages I use on my home computer

Requires: steam
Requires: wine
Requires: inkscape
Requires: youtube-dl
Requires: vlc

%description home
Packages I use on my home computer

%files -n %{name}-home

%changelog
{{{ git_dir_changelog }}}
