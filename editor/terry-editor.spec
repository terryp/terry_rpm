Name: terry-editor
Version: {{{ git_dir_version }}}
Release: 1%{?dist}
Summary: Packages required to get my editor config working
BuildArch: noarch
VCS: {{{ git_dir_vcs }}}

License: MIT

Requires: terry-repos
Requires: neovim
Requires: yamllint
Requires: xclip
Requires: rustfmt
Requires: python3-isort
Requires: python3-flake8
Requires: python3-neovim
Requires: ShellCheck
Requires: gitlint
Requires: python3-jedi

# A neovim ext uses sqlite
Requires: sqlite
Requires: sqlite-devel

Recommends: rust-src

%description
Packages to configure my editor.

%files -n terry-editor

%changelog
{{{ git_dir_changelog }}}
