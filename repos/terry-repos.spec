Name: terry-repos
Version: {{{ git_dir_version }}}
Release: 1%{?dist}
Summary: Repos I use.
BuildArch: noarch
VCS: {{{ git_dir_vcs }}}

License: MIT
Source1: _copr:copr.fedorainfracloud.org:frostyx:qtile.repo
Source2: dropbox.repo

%description
RPM Repos I install packages from.

%prep
echo "Nothing to prep"

%build
echo "Nothing to build"

%install
install -d -m755 %{buildroot}%{_sysconfdir}/yum.repos.d

# Yum .repo files
%{__install} -p -m644 \
    %{SOURCE1} \
    %{SOURCE2} \
    %{buildroot}%{_sysconfdir}/yum.repos.d


%files
%config(noreplace) %{_sysconfdir}/yum.repos.d/*

%changelog
{{{ git_dir_changelog }}}
